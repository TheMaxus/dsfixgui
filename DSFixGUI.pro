# This file is part of DSFixGUI.
#
#   DSFixGUI is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   DSFixGUI is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with DSFixGUI.  If not, see <https://www.gnu.org/licenses/>.

#-------------------------------------------------
#
# Project created by QtCreator 2016-07-27T16:33:18
#
#-------------------------------------------------

QT       += core gui widgets

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

TARGET = DSFixGUI
TEMPLATE = app

CONFIG += c++17

SOURCES += src/main.cpp\
        src/file.cpp \
        src/mainwindow.cpp

HEADERS  += src/mainwindow.h \
         src/file.h

FORMS    += src/mainwindow.ui
