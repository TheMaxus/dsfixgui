/* This file is part of DSFixGUI.
 *
 *   DSFixGUI is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   DSFixGUI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with DSFixGUI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "file.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QtCore>
#include <QtGui>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    openFile(filename);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_actionAbout_Qt_triggered() {
    QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::on_actionVersion_triggered() {
    QMessageBox::about(
                this, "About",
                "<h3>About DSFixGUI</h3>"
                "<p>Version alpha 0.01</p>"
                "<p>This Program is licensed under <a "
                "href=\"https://www.gnu.org/licenses/gpl-3.0.html\"> GNU GPLv3</p>");
}

void MainWindow::on_pushButton_load_clicked() { openFile(getFileName()); }

void MainWindow::openFile(QString filename) {
    try {
        File file(filename, disableHashChecking);
        settings = file.readFile();
    } catch (QString errorMessage) {
        QMessageBox::critical(this, "Error", errorMessage);
    }

    if (validateSettings())
        loadSettings();
}

QString MainWindow::getFileName() {
    QString newFilename = QFileDialog::getOpenFileName(
                              this, "Choose ini file...", "DSfix.ini", ".ini file (*.ini*)");
    return newFilename.isEmpty() ? filename : newFilename;
}

// Check if settings in file are valid
bool MainWindow::validateSettings() {
    bool otherOptionsValid = true;
    // Check if all booleans are either 0 or 1
    QStringList booleans = {"unlockFPS",
                            "disableDofScaling",
                            "borderlessFullscreen",
                            "disableCursor",
                            "captureCursor",
                            "enableHudMod",
                            "enableMinimalHud",
                            "enableBackups",
                            "enableTextureDumping",
                            "enableTextureOverride",
                            "skipIntro",
                            "hudScaleFactor"};
    for (const auto &x : booleans) {
        otherOptionsValid = otherOptionsValid && (settings[x] == "1" || settings[x] == "0");
    }


    auto opacitySettings = parseOpatityoptions();
    for (const auto &x : opacitySettings) {
        otherOptionsValid = otherOptionsValid && inBounds<float>(x, 0, 1);
    }

    return (inBounds(settings["aaQuality"].toInt(), 0, 4) &&
            inBounds(settings["ssaoStrength"].toInt(), 0, 3) &&
            inBounds(settings["ssaoScale"].toInt(), 1, 3) &&
            inBounds(settings["dofBlurAmount"].toInt(), 0, 4) &&
            inBounds(settings["filteringOverride"].toInt(), 0, 2) &&
            inBounds(settings["logLevel"].toInt(), 0, 11)) &&
            otherOptionsValid;
}

template <typename T> bool MainWindow::inBounds(T value, T lower, T upper) {
    return value >= lower && value <= upper;
}

// Assigns values to UI elements
void MainWindow::loadSettings() {
    ui->spinBox_renderResWidth->setValue(settings["renderWidth"].toInt());
    ui->spinBox_renderResHight->setValue(settings["renderHeight"].toInt());

    ui->spinBox_screenResWidth->setValue(settings["presentWidth"].toInt());
    ui->spinBox_screenResHeight->setValue(settings["presentHeight"].toInt());

    ui->checkbox_Unlockfps->setChecked(settings["unlockFPS"].toInt());
    ui->spinbox_FPSlimit->setValue(settings["FPSlimit"].toInt());
    ui->spinbox_FPSthreshold->setValue(settings["FPSthreshold"].toInt());

    ui->comboBox_AAq->setCurrentIndex(settings["aaQuality"].toInt());
    ui->comboBox_AAt->setCurrentIndex(
                ui->comboBox_AAt->findText(settings["aaType"]));

    ui->comboBox_SSAOstrength->setCurrentIndex(settings["ssaoStrength"].toInt());
    ui->comboBox_SSAOscale->setCurrentIndex(settings["ssaoScale"].toInt() - 1);
    ui->comboBox_SSAOtype->setCurrentIndex(
                ui->comboBox_SSAOtype->findText(settings["ssaoType"]));

    ui->spinbox_dofOverride->setValue(settings["dofOverrideResolution"].toInt());
    ui->checkBox_dofScalingOverride->setChecked(
                settings["disableDofScaling"].toInt());
    ui->spinBox_dofBlur->setValue(settings["dofBlurAmount"].toInt());

    ui->comboBox_filteringOverride->setCurrentIndex(
                settings["filteringOverride"].toInt());

    ui->checkBox_borderlessFullscreen->setChecked(
                settings["borderlessFullscreen"].toInt());
    ui->checkBox_captureCursor->setChecked(settings["captureCursor"].toInt());
    ui->checkBox_disableCursor->setChecked(settings["disableCursor"].toInt());

    ui->checkBox_hudMod->setChecked(settings["enableHudMod"].toInt());
    ui->checkBox_minimalHud->setChecked(settings["enableMinimalHud"].toInt());
    ui->spinBox_hudScale->setValue(
                int(settings["hudScaleFactor"].toFloat() * 100));
    QVector<float> opacitySettings = parseOpatityoptions();
    ui->spinBox_topLeft->setValue(int(opacitySettings[0] * 100));
    ui->spinBox_bottomLeft->setValue(int(opacitySettings[1] * 100));
    ui->spinBox_bottomRight->setValue(int(opacitySettings[2] * 100));

    ui->checkBox_textureDumping->setChecked(
                settings["enableTextureDumping"].toInt());
    ui->checkBox_textureOverride->setChecked(
                settings["enableTextureOverride"].toInt());

    ui->checkBox_skipIntro->setChecked(settings["skipIntro"].toInt());

    ui->checkBox_saveBackup->setChecked(settings["enableBackups"].toInt());
    ui->spinbox_backupInterval->setValue(settings["backupInterval"].toInt());
    ui->spinbox_maxBackups->setValue(settings["maxBackups"].toInt());

    ui->spinBox_loggingLevel->setValue(settings["logLevel"].toInt());

    ui->screenshotDirLine->setText(settings["screenshotDir"]);

    QStringList languageList = {"none", "en-GB", "fr",    "it", "de",
                                "es",   "ko",    "zh-tw", "pr", "ru"};
    ui->comboBox_language->setCurrentIndex(
                languageList.indexOf(settings["overrideLanguage"]));

    ui->inputWrapperLine->setText(settings["dinput8dllWrapper"]);
}

// Remove "f" at the end of the number, because QString::toFloat() doesn't
// understand it
QVector<float> MainWindow::parseOpatityoptions() {
    QStringList opacityOptions = {"hudTopLeftOpacity", "hudBottomLeftOpacity",
                                  "hudBottomRightOpacity"};
    QVector<float> output;
    for (const auto &x : opacityOptions) {
        output.append(settings[x].remove("f").toFloat());
    }
    return output;
}

void MainWindow::on_actionOpen_triggered() { openFile(getFileName()); }

void MainWindow::on_actionDisable_hash_checking_toggled(bool arg1) {
    disableHashChecking = arg1;
}

// Assign settings to UI elements

// Graphics tab

void MainWindow::on_spinBox_renderResHight_valueChanged(const QString &arg1) {
    settings["renderHeight"] = arg1;
}

void MainWindow::on_pushButton_save_clicked() {
    File file(filename, disableHashChecking);
    ui->statusBar->showMessage(file.writeFile(settings));
    openFile(filename);
}

void MainWindow::on_spinBox_screenResWidth_valueChanged(const QString &arg1) {
    settings["presentWidth"] = arg1;
}

void MainWindow::on_spinBox_screenResHeight_valueChanged(const QString &arg1) {
    settings["presentHeight"] = arg1;
}

void MainWindow::on_comboBox_AAq_currentIndexChanged(int index) {
    settings["aaQuality"] = QString::number(index);
}

void MainWindow::on_spinbox_FPSlimit_valueChanged(const QString &arg1) {
    settings["FPSlimit"] = arg1;
}

void MainWindow::on_checkbox_Unlockfps_toggled(bool checked) {
    settings["unlockFPS"] = QString::number(checked);
}

void MainWindow::on_spinbox_FPSthreshold_valueChanged(const QString &arg1) {
    settings["FPSthreshold"] = arg1;
}

void MainWindow::on_comboBox_AAt_currentIndexChanged(const QString &arg1) {
    settings["aaType"] = arg1;
}

void MainWindow::on_comboBox_SSAOtype_currentIndexChanged(const QString &arg1) {
    settings["ssaoType"] = arg1;
}

void MainWindow::on_comboBox_SSAOstrength_currentIndexChanged(int index) {
    settings["ssaoStrength"] = QString::number(index);
}

void MainWindow::on_comboBox_SSAOscale_currentIndexChanged(int index) {
    settings["ssaoScale"] = QString::number(index + 1);
}

void MainWindow::on_comboBox_filteringOverride_currentIndexChanged(int index) {
    settings["filteringOverride"] = QString::number(index);
}

void MainWindow::on_checkBox_borderlessFullscreen_toggled(bool checked) {
    settings["borderlessFullscreen"] = QString::number(checked);
}

void MainWindow::on_checkBox_disableCursor_toggled(bool checked) {
    settings["disableCursor"] = QString::number(checked);
}

void MainWindow::on_checkBox_captureCursor_toggled(bool checked) {
    settings["captureCursor"] = QString::number(checked);
}

void MainWindow::on_spinbox_dofOverride_valueChanged(const QString &arg1) {
    settings["dofOverrideResolution"] = arg1;
}

void MainWindow::on_spinBox_dofBlur_valueChanged(const QString &arg1) {
    settings["dofBlurAmount"] = arg1;
}

void MainWindow::on_checkBox_dofScalingOverride_toggled(bool checked) {
    settings["disableDofScaling"] = QString::number(checked);
}

// UI tab

void MainWindow::on_checkBox_hudMod_toggled(bool checked) {
    ui->groupBox_opacity->setEnabled(checked);
    ui->horizontalSlider_hudScale->setEnabled(checked);
    ui->spinBox_hudScale->setEnabled(checked);
    ui->checkBox_minimalHud->setEnabled(checked);

    settings["enableHudMod"] = QString::number(checked);
}

void MainWindow::on_checkBox_minimalHud_toggled(bool checked) {
    settings["enableMinimalHud"] = QString::number(checked);
}

void MainWindow::on_spinBox_hudScale_valueChanged(int arg1) {
    settings["hudScaleFactor"] = QString::number(double(arg1) / 100);
}

void MainWindow::on_spinBox_topLeft_valueChanged(int arg1) {
    settings["hudTopLeftOpacity"] = QString::number(double(arg1) / 100) + "f";
}

void MainWindow::on_spinBox_bottomLeft_valueChanged(int arg1) {
    settings["hudBottomLeftOpacity"] = QString::number(double(arg1) / 100) + "f";
}

void MainWindow::on_spinBox_bottomRight_valueChanged(int arg1) {
    settings["hudBottomRightOpacity"] = QString::number(double(arg1) / 100) + "f";
}

void MainWindow::on_spinBox_renderResWidth_valueChanged(const QString &arg1) {
    settings["renderWidth"] = arg1;
}

// Other tab
void MainWindow::on_checkBox_textureDumping_toggled(bool checked) {
    settings["enableTextureDumping"] = QString::number(checked);
}

void MainWindow::on_checkBox_textureOverride_toggled(bool checked) {
    settings["enableTextureOverride"] = QString::number(checked);
}

void MainWindow::on_spinBox_loggingLevel_valueChanged(const QString &arg1) {
    settings["logLevel"] = arg1;
}

void MainWindow::on_screenshotDirButton_clicked() {
    QString dir =
            QFileDialog::getExistingDirectory(this, "Choose directory...", ".");
    dir = dir.isEmpty() ? "." : dir;
    ui->screenshotDirLine->setText(dir);
    settings["screenshotDir"] = dir;
}

void MainWindow::on_inputWrapperButton_clicked() {
    QString wrapperInput = QFileDialog::getOpenFileName(
                               this, "Choose input wrapper", ".", "Dynamic Link Library .dll (*.dll)");
    if (wrapperInput.isEmpty()) {
        wrapperInput = "none";
    } else {
        settings["dinput8dllWrapper"] = wrapperInput;
    }
    ui->inputWrapperLine->setText(wrapperInput);
}

void MainWindow::on_checkBox_skipIntro_toggled(bool checked) {
    settings["skipIntro"] = QString::number(checked);
}

void MainWindow::on_comboBox_language_currentIndexChanged(int index) {
    QStringList languageList = {"none", "en-GB", "fr",    "it", "de",
                                "es",   "ko",    "zh-tw", "pr", "ru"};
    settings["overrideLanguage"] = languageList.at(index);
}

void MainWindow::on_checkBox_saveBackup_toggled(bool checked) {
    settings["enableBackups"] = QString::number(checked);
}

void MainWindow::on_spinbox_backupInterval_valueChanged(const QString &arg1) {
    settings["backupInterval"] = arg1;
}

void MainWindow::on_spinbox_maxBackups_valueChanged(const QString &arg1) {
    settings["maxBackups"] = arg1;
}
