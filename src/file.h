/* This file is part of DSFixGUI.
 *
 *   DSFixGUI is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   DSFixGUI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with DSFixGUI.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILE_H
#define FILE_H

#include <QMap>
#include <QString>

class File {
public:
    File(QString inputFile, bool hashSetting);
    QMap<QString, QString> readFile();
    QString writeFile(const QMap<QString, QString> &);

private:
    bool disableHashChecking;
    bool checkHash(const QStringList &);
    bool checkForChanges(const QByteArray &);
    void loadFile(const QString &);
    QByteArray fileHash;
    QString filename = "DSfix.ini";
    QStringList lines;
};

#endif // FILE_H
