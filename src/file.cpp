/* This file is part of DSFixGUI.
 *
 *   DSFixGUI is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   DSFixGUI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with DSFixGUI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "file.h"
#include <QCryptographicHash>
#include <QFile>
#include <QSaveFile>

File::File(QString inputFile, bool hashSetting)
    : disableHashChecking(hashSetting), filename(inputFile) {
    loadFile(filename);
}

// Load file into memory
void File::loadFile(const QString &filename) {
    QFile file(filename);
    QByteArray textFile = {};
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw file.errorString();

    textFile = file.readAll();
    file.close();
    fileHash = QCryptographicHash::hash(textFile, QCryptographicHash::Md5);
    lines = QString(textFile).split("\n");
    lines.pop_back(); // Remove extra line that appears after splitting text
}

// Removes comments and empety strings and returns settings in QMap
QMap<QString, QString> File::readFile() {
    QMap<QString, QString> settings = {};
    for (const auto &x : lines) {
        if (x.startsWith("#") || x.isEmpty())
            continue;
        QStringList setting = x.split(" ");
        settings.insert(setting[0], setting[1]);
    }
    if (checkHash(settings.keys()))
        return settings;

    throw QString("Invalid .ini file");
}

QString File::writeFile(const QMap<QString, QString> &settings) {
    for (auto &line : lines) {
        if (line.startsWith("#") && line.isEmpty())
            continue;
        QStringList setting = line.split(" ");
        setting[1] = settings[setting[0]];
        line = setting[0] + " " + setting[1];
    }

    QSaveFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        throw file.errorString();

    QByteArray saveFile = {};
    for (const auto &x : lines) {
        saveFile.append(x.toUtf8() + "\n");
    }
    if (!checkForChanges(saveFile))
        return "No changes were made!";
    file.write(saveFile);
    file.commit();
    return "Save succesful";
}

bool File::checkHash(const QStringList &keys) {
    if (disableHashChecking)
        return true;
    const QByteArray defaultSettingNameMD5 = "f09bf93e5969584e232008f95763d6ca";
    QByteArray string = {};
    for (const auto &x : keys) {
        string.append(x.toUtf8());
    }
    QByteArray settingNameMD5 = {};
    settingNameMD5 =
            QCryptographicHash::hash(string, QCryptographicHash::Md5).toHex();
    return defaultSettingNameMD5 == settingNameMD5;
}

bool File::checkForChanges(const QByteArray &saveFile) {
    QByteArray editedHash =
            QCryptographicHash::hash(saveFile, QCryptographicHash::Md5);
    return editedHash != fileHash;
}
