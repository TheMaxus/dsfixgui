/* This file is part of DSFixGUI.
 *
 *   DSFixGUI is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   DSFixGUI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with DSFixGUI.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionAbout_Qt_triggered();
    void on_actionVersion_triggered();
    void on_screenshotDirButton_clicked();
    void on_inputWrapperButton_clicked();
    void on_pushButton_load_clicked();
    void on_actionOpen_triggered();
    void on_actionDisable_hash_checking_toggled(bool arg1);
    void on_spinBox_hudScale_valueChanged(int arg1);
    void on_spinBox_topLeft_valueChanged(int arg1);
    void on_spinBox_bottomLeft_valueChanged(int arg1);
    void on_spinBox_bottomRight_valueChanged(int arg1);
    void on_spinBox_renderResWidth_valueChanged(const QString &arg1);
    void on_spinBox_renderResHight_valueChanged(const QString &arg1);
    void on_pushButton_save_clicked();
    void on_spinBox_screenResWidth_valueChanged(const QString &arg1);
    void on_spinBox_screenResHeight_valueChanged(const QString &arg1);
    void on_comboBox_AAq_currentIndexChanged(int index);
    void on_spinbox_FPSlimit_valueChanged(const QString &arg1);
    void on_checkBox_hudMod_toggled(bool checked);
    void on_checkBox_minimalHud_toggled(bool checked);
    void on_checkbox_Unlockfps_toggled(bool checked);
    void on_spinbox_FPSthreshold_valueChanged(const QString &arg1);
    void on_comboBox_AAt_currentIndexChanged(const QString &arg1);
    void on_comboBox_SSAOtype_currentIndexChanged(const QString &arg1);
    void on_comboBox_SSAOstrength_currentIndexChanged(int index);
    void on_comboBox_SSAOscale_currentIndexChanged(int index);
    void on_comboBox_filteringOverride_currentIndexChanged(int index);
    void on_checkBox_borderlessFullscreen_toggled(bool checked);
    void on_checkBox_disableCursor_toggled(bool checked);
    void on_checkBox_captureCursor_toggled(bool checked);
    void on_spinbox_dofOverride_valueChanged(const QString &arg1);
    void on_spinBox_dofBlur_valueChanged(const QString &arg1);
    void on_checkBox_dofScalingOverride_toggled(bool checked);
    void on_checkBox_textureDumping_toggled(bool checked);
    void on_checkBox_textureOverride_toggled(bool checked);
    void on_spinBox_loggingLevel_valueChanged(const QString &arg1);
    void on_checkBox_skipIntro_toggled(bool checked);
    void on_comboBox_language_currentIndexChanged(int index);
    void on_checkBox_saveBackup_toggled(bool checked);
    void on_spinbox_backupInterval_valueChanged(const QString &arg1);
    void on_spinbox_maxBackups_valueChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QMap<QString, QString> settings;
    QString filename = "DSfix.ini";
    void loadSettings();
    bool validateSettings();
    template <typename T> bool inBounds(T, T, T);
    void openFile(QString);
    void enableHudModUi(bool);
    bool disableHashChecking = false;
    QString getFileName();
    QVector<float> parseOpatityoptions();
};

#endif // MAINWINDOW_H
